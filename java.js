var numChapter = 0; var nPage = 1; var classChapter; var intro = 0;
var CHAPTER_ARR0 = ["בתחילת השיעור שלכם אתם מתכננים לספר סיפור נחמד שסיפרו לכם פעם. בואו ניזכר בסיפור:", "אתם מתכוננים לשיעור, מלאי כוונות טובות להגיע מוכנים, אבל האם אתם באמת זוכרים את כל הפרטים הקטנים? בואו נראה אתכם!"];
var TITLE_ARR = ["סיפורו של שיעור", "עכשיו תורכם"];
// -------- load function ---------------
// Description: Displays introspeech
// Parameters: event
// Programmer: Maria Dragilev
// Date: 20.04.2021
// ----------------------------------- 
$(function () {
    console.log(CHAPTER_ARR0[1]);
    $("#btn-kurse").on('click', kurseMap);
    $("#btn-notkurse").on("click", notkursemap);
});

// -------- kurseMap ---------------
// Description: opens the map
// Parameters: event
// Programmer: Maria Dragilev.
// Date: 25.04.2021
function kurseMap(event) {
    console.log("in");

    //if its the first time that they see the map.
    if (nPage === 1) {
        $(".main").hide();
        setTimeout(function () {
            $(".instructions").show();
        }, 2000);
        $("#x").on("click",closeMap);
    }
    $(".map").show();
    $(".map").addClass(" animate__animated animate__backInDown");
    // $('.mapIcon').show();
    $('.map').css("display", "inline-block");
    $("#x").on("click", closeMap);
    $(".chapter0").on("click", addInfromation);
}

function nextPage(event) {
    nPage++;
    console.log("page"+nPage);
    addInfromation();
}

function notkursemap(event) {

}

function listenersChapter() {

}

// -------- addInfromation ---------------
// Description: adds the right text and title acording to the page.
// Parameters: event
// Programmer: Maria Dragilev.
// Date: 25.04.2021
function addInfromation(event) {
    closeMap();
   
    console.log("page"+nPage);
    // console.log(eval("CHAPTER_ARR"+classChapter)[nPage-1]);
    
    //if its the first page
    if(nPage === 1) {
        classChapter = this.className.slice(-1);
        setTimeout(() => {
            console.log(TITLE_ARR[nPage-1]);
            $(".title").text(TITLE_ARR[nPage-1]);
            $(".main-text").text(eval("CHAPTER_ARR"+classChapter)[nPage-1]);
            $(".page1").show();
            $("#nextBtn").show();
            $("#nextBtn").on("click",nextPage);
        },5000);
    }

    //all the pages
    else if(nPage === 2) {
        $(".title").text(TITLE_ARR[nPage-1]);
        $(".main-text").text(eval("CHAPTER_ARR"+classChapter)[nPage-1]);
        $(".page"+(nPage-1)).hide();
        $(".page"+nPage).show();
        }
    else if(nPage >=1) {
        // console.log(".page"+(nPage-1));
        $(".page"+(nPage-1)).hide();
        console.log("showww");
        $(".page"+nPage-2).hide();
    }
}

// -------- closeMap ---------------
// Description: closing the map ans shows the map  icon.
// Parameters: event
// Programmer: Maria Dragilev
// Date: 20.04.2021
// ----------------------------------- 
function closeMap() {
    if (nPage === 1) {
        console.log("dvdvdvdvdvdddddddddddddddddd");
        $(".instructions").hide();
        $(".map").animate({
            right: '2vw',
            top: '1vw',
            width: '0vw',
            height: '0vw',
            fontSize: '0.2vw'
        }, 500);
        setTimeout(function () {
            $(".map").hide(200)
        }, 400);
        $(".map-Title").animate({
            fontSize: '0.2vw'
        });
        
        setTimeout(function () {
            $(".mapIcon").show(700);
           setTimeout(function(){
            $(".mapinstructions").show();
           },1500);
        }, 700);
        setTimeout(function () {
            $(".mapinstructions").hide();
        }, 4500);
    }
    else {
        $(".map").hide();
    }
}